
<section class="sptb">
    <div class="container ">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12">
                    <div class="items-gallery">
                        <div class="items-blog-tab text-left">
                            <div class="items-blog-tab-heading  center-block text-left">
                                <h2>Latest Cars</h2><br>
                               
				
					
					
					
							
								<div class="col-12">
									<ul class="nav items-blog-tab-menu text-left">
										<li class=""><a href="#tab-1" class="active show" data-toggle="tab">All</a></li>
										
										<li><a href="#tab-3" data-toggle="tab" class="">Volvo</a></li>
										<li><a href="#tab-3" data-toggle="tab" class="">Porsche</a></li>
										<li><a href="#tab-3" data-toggle="tab" class="">Kia</a></li>
										
									</ul><br>
								</div>
							</div>
					
						
				<div id="myCarousel2" class="owl-carousel owl-carousel-icons2">
					<!-- Wrapper for carousel items -->
					
						
						<div class="card mb-0">
							<div class="item-card7-imgs">
								<a class="link" href="cars.html"></a>
								<img src="<?php echo base_url(); ?>/assets/images/media/others/a1.jpg" alt="img" class="cover-image">
								
							</div>
							<div class="card-body">
								<div class="item-card7-desc">
									<div class="item-card7-text">
										<a href="cars.html" class="text-dark"><h4 class="">Porsche Taycan</h4></a>
									</div>
									
									<p class="mb-0">Rs 1.50 Cr*</p>
								</div>
								
							</div>
							<a class="btn btn-primary btn-lg " href="#">View Details</a>
						</div>
					
					<div class="item">
						<div class="card mb-0">
							
							<div class="item-card7-imgs">
								<a class="link" href="cars.html"></a>
								<img src="<?php echo base_url(); ?>/assets/images/media/others/a2.jpg" alt="img" class="cover-image">
								
								
							</div>
							<div class="card-body">
								<div class="item-card7-desc">
									<div class="item-card7-text">
										<a href="cars.html" class="text-dark"><h4 class="">Porsche Macan</h4></a>
									</div>
									
									<p class="mb-0">Rs 83.21 Lakh</p>
								</div>
								
							</div>
							<a class="btn btn-primary btn-lg " href="#">View Details</a>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							
							<div class="item-card7-imgs">
								<a class="link" href="cars.html"></a>
								<img src="<?php echo base_url(); ?>/assets/images/media/others/a3.jpg" alt="img" class="cover-image">
								<div class="item-card7-overlaytext">
									
								</div>
								
							</div>
							<div class="card-body">
								<div class="item-card7-desc">
									<div class="item-card7-text">
										<a href="cars.html" class="text-dark"><h4 class="">Volvo XC90</h4></a>
									</div>
									
									<p class="mb-0">Rs 89.90 Lakh - 1.31 Cr*</p>
								</div>
								
							</div>
							<a class="btn btn-primary btn-lg " href="#">View Details</a>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							
							<div class="item-card7-imgs">
								<a class="link" href="cars.html"></a>
								<img src="<?php echo base_url(); ?>/assets/images/media/others/a4.jpg" alt="img" class="cover-image">
								<div class="item-card7-overlaytext">
									
								</div>
								
							</div>
							<div class="card-body">
								<div class="item-card7-desc">
									<div class="item-card7-text">
										<a href="cars.html" class="text-dark"><h4 class="">Maruti Celerio</h4></a>
									</div>
									
									<p class="mb-0">Rs 4.99 - 6.94 Lakh*</p>
								</div>
								
							</div>
							<a class="btn btn-primary btn-lg " href="#">View Details</a>
						</div>
					</div>
					
				</div>
			</div>
		</section>